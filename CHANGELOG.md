# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- allow for using stdin/stdout instead of files
- print an example data file
- use tests

## [1.0.0] - 2021-01-29
### Added
- OpenTripPlanner API script

[1.0.0]: https://gitlab.com/OleMussmann/otp-python/-/releases/v1.0.0
