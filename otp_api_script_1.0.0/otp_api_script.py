#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Main script for OpenTripPlanner API batch jobs.

The OTP API script takes a data file or stream as an input and makes
asynchroneous calls to the OpenTripPlanner API.

An overview of all options will be printed with the `--help` option.

Examples:
    $ otp --help
    $ otp INPUT_FILE
    $ otp --version 1.0.0 INPUT_FILE

"""

import argparse
import configparser
import json
import os
import sys
from datetime import datetime
from pathlib import Path, PosixPath
from typing import IO, Dict, Iterator, List, Optional, Tuple

import packaging.version
import utils

# What character(s) to use as comments in the data file.
COMMENT = "#"
# Separates the settings block from the header in the data file.
END_SETTINGS = "### END SETTINGS ###"
# Basename of the folder where this script resides in.
SCRIPT_FOLDER_NAME = "otp_api_script"
# Name of the symlink in the PATH
GLOBAL_SCRIPT_NAME = "otp"


def parse_arguments() -> argparse.Namespace:
    """Parse command-line arguments.

    Creates command-line arguments and parses them, taking care of missing
    options. Also compiles a `--help` function.

    Returns:
        argparse.Namespace: Dictionary-style collection of command-line
                            arguments and options.
    """
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
        description="Make parallel calls to the OpenTripPlanner API.")

    parser.add_argument(
        "input_file",
        metavar="INPUT_FILE",
        help="name of the data file to parse")

    parser.add_argument(
        "-o", "--output",
        metavar="FILE",
        help="name of the output file; \
            default is INPUT_FILE.OUTPUT.xyz in the same folder as the input \
            file.")

    parser.add_argument(
        "-t", "--test",
        action='store_true',
        help="only assemble and print the URLs")

    parser.add_argument(
        "--version",
        help="which version to run, defaults to 'latest'")

    parser.add_argument(
        "--print_versions",
        nargs=0,
        action=PrintVersions,
        help="print all available versions and exit")

    return parser.parse_args()


def print_versions() -> None:
    """Print all available OTP API script versions."""
    # Get the path of this file.
    path: PosixPath = Path(__file__)

    # Find the target of "latest" version.
    latest_symlink: str = str(path.parent.parent) + \
        "/" + SCRIPT_FOLDER_NAME + "_latest"
    latest_folder: PosixPath = Path(latest_symlink).resolve()
    latest_version: str = \
        utils.get_version_from_folder_name(latest_folder, SCRIPT_FOLDER_NAME)

    print("available versions:")
    versions: list = \
        [utils.get_version_from_folder_name(folder, SCRIPT_FOLDER_NAME)
         for folder in os.listdir(path.parent.parent)
         if SCRIPT_FOLDER_NAME in folder]
    versions.remove("latest")
    # Sort by semantic versioning.
    versions.sort(reverse=True, key=packaging.version.parse)
    version: str
    for version in versions:
        if version == latest_version:
            print(version + " (latest)")
        else:
            print(version)


class PrintVersions(argparse.Action):
    """Create argparse.Action to quit parser after print_versions()."""

    # pylint: disable=too-few-public-methods
    def __call__(
            self,
            parser: argparse.ArgumentParser,
            namespace: argparse.Namespace,
            values: list,
            option_string: Optional[str] = None) -> None:
        """Quit parser after print_versions()."""
        print_versions()
        parser.exit()


def get_settings_string(start_time: datetime, settings: str) -> str:
    """Assemble the settings of the current call of the OTP API script.

    Args:
        start_time (datetime): Script run startet at this time.
        settings (str): Settings from the data file.

    Returns:
        str: Concatenated and formatted settings.
    """
    script_version: str = utils.get_this_script_version(SCRIPT_FOLDER_NAME)
    sys_args: List[str] = sys.argv[1:]
    line: List[str]

    # Remove empty lines and comments from settings
    stripped_settings_list: List[str] = \
        [line for line in settings.split('\n') if
         (line.strip() != "" and not line.startswith(COMMENT))]

    s: List[str] = []  # pylint: disable=invalid-name
    s.append(COMMENT * 3 + " RUN STARTED: " + str(start_time) + " " +
             COMMENT * 3)
    s.append(COMMENT)
    s.append(COMMENT * 3 + " SCRIPT VERSION: " + script_version + " " +
             COMMENT * 3)
    s.append(COMMENT)
    s.append(COMMENT * 3 + " FILE SETTINGS: " + COMMENT * 3)
    s.extend([COMMENT + " " + line for line in stripped_settings_list])
    s.append(COMMENT)
    s.append(COMMENT * 3 + " RESULTS CAN BE REPRODUCED WITH: " + COMMENT * 3)
    s.append(COMMENT + " " + utils.get_cli_command(GLOBAL_SCRIPT_NAME,
                                                   sys_args, script_version))
    s.append(COMMENT)

    return "\n".join(s)


def only_print_urls(settings: str, args: argparse.Namespace, data_header: str,
                    config: configparser.SectionProxy) -> None:
    """Parse the data file and print assembled URLs.

    First, print the start time, settings of the data file and the CLI
    parameters. Then, for each data row, print the raw data, the parsed URL
    parameters and the assembled URL. Finish with the end time.

    Args:
        settings (str): Settings from the data file.
        args (argparse.Namespace): Parsed CLI arguments.
        data_header (str): Header of the data file.
        config (configparser.SectionProxy): Parsed configurations from the data
            file.
    """
    time_start_run: datetime = datetime.now()
    print(get_settings_string(time_start_run, settings))
    print(COMMENT * 3 + " header: " + COMMENT * 3)
    print(COMMENT + " " + data_header)
    print(COMMENT)

    for data_row in utils.file_to_params(args.input_file, comment=COMMENT,
                                         end_settings=END_SETTINGS,
                                         config=config):
        print(COMMENT * 3 + " data: " + COMMENT * 3)
        print(COMMENT + " " + str(data_row["data"].rstrip('\n')))
        print(COMMENT * 3 + " URL params: " + COMMENT * 3)
        print(COMMENT + " " + str(data_row["url_params"]))
        print(COMMENT * 3 + " URL: " + COMMENT * 3)
        print(str(data_row["url"]))
        print(COMMENT)

    print(COMMENT * 3 + " RUN ENDED : " + str(datetime.now()) + " " +
          COMMENT * 3)


def write_api_responses_to_file(settings: str,
                                args: argparse.Namespace,
                                data_header: str,
                                config: configparser.SectionProxy) -> None:
    """Query the OTP API and write the responses to file.

    Create generators for the data rows and the API output. This makes sure we
    only read into (and keep in) memory the chunks of data that is needed right
    now. Then, loop in batches through the generators and write the results to
    file.

    Args:
        settings (str): Settings from the data file.
        args (argparse.Namespace): Parsed CLI arguments.
        data_header (str): Header of the data file.
        config (configparser.SectionProxy): Parsed configurations from the data
            file.
        comment (str): Character(s) used as comments in the data file.
    """
    time_start_run: datetime = datetime.now()
    separator: str = json.loads(config["separator"])

    output_filename: str = utils.get_output_filename(args)

    if os.path.isfile(output_filename):
        raise OSError("Output file " + output_filename + " already exists.")

    f: IO  # pylint: disable=invalid-name
    with open(output_filename, "w") as f:  # pylint: disable=invalid-name
        f.write(get_settings_string(time_start_run, settings))
        f.write("\n")
        f.write(COMMENT * 3 + " HEADER: " + COMMENT * 3 + "\n")
        f.write(data_header + separator + "url" + separator + "response\n")
        f.write(COMMENT * 3 + " DATA LINES: " + COMMENT * 3 + "\n")

    # `data_rows` and `output` are a generator, yielding results.
    # This makes is such that blocks of data are only read
    # to memory (and then discarded) when they are actually used.
    data_rows: Iterator[List[str]] = \
        utils.file_to_params(args.input_file, comment=COMMENT,
                             end_settings=END_SETTINGS, config=config)
    output: Iterator[Tuple[int, Dict[str, str]]] = \
        utils.async_query(data_rows, config=config)

    # Repeatedly opening and closing the file is not efficient, but is safer
    # for the integrity of the file. Working in larger batches minimizes this
    # overhead.
    while True:
        try:
            data_batch = next(output)
            for line in data_batch:
                # pylint: disable=invalid-name
                with open(output_filename, "a") as f:
                    f.write(line + "\n")
        except StopIteration:
            break

    time_end_run = datetime.now()
    with open(output_filename, "a") as f:  # pylint: disable=invalid-name
        f.write(COMMENT * 3 + " RUN ENDED: " + str(time_end_run) + " " +
                COMMENT * 3)


def main() -> None:
    """Run the OTP API script."""
    args: argparse.Namespace = parse_arguments()

    settings: str
    data_header: str
    settings, data_header = \
        utils.parse_header(args.input_file, END_SETTINGS, COMMENT)

    config: configparser.ConfigParser = configparser.ConfigParser()
    config.read_string("[otp]\n" + settings)  # add section header

    if args.test:
        only_print_urls(settings, args, data_header, config["otp"])
    else:
        write_api_responses_to_file(settings, args, data_header, config["otp"])


if __name__ == "__main__":
    main()
