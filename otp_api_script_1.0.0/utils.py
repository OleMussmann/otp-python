#!/usr/bin/env python3

"""Utilities for OTP API script."""

import argparse
import asyncio
import configparser
import json
import logging
import os
import urllib
from pathlib import Path, PosixPath
from typing import IO, Coroutine, Dict, Iterator, List, Tuple

import aiohttp
import areq

logger = areq.logger
logger.setLevel(logging.WARNING)


def assemble_url(params: List[Tuple],
                 config: configparser.ConfigParser) -> str:
    """Concatenates a URL string for querying a OpenTripPlanner REST API.

    Args:
        params (List[Tuple]): Dictionary containing the query parameters.
        config (configparser.ConfigParser): Configuration object containing
            server parameters.
    Returns:
        str: Assembled URL.
    """
    protocol = json.loads(config["protocol"])
    server = json.loads(config["server"])
    port = str(json.loads(config["port"]))
    stub = json.loads(config["stub"])
    router = json.loads(config["router"])
    target = json.loads(config["target"])
    base_url: str = protocol + "://" + server + ":" + port + stub + \
        router + "/" + target + "?"

    return base_url + urllib.parse.urlencode(params)


def get_version_from_folder_name(folder_name: str,
                                 script_folder_name: str) -> str:
    """Derive the script version from the name of a folder.

    Args:
        folder_name (str): Full folder name to analyze.
        script_folder_name (str): Base name of the script folder.

    Returns:
        str: Script version.
    """
    return str(folder_name).split(script_folder_name)[-1].lstrip("_")


def get_this_script_version(script_folder_name: str) -> str:
    """Derive the script version from a folder name.

    Args:
        script_folder_name (str): Derive from this folder.

    Returns:
        str: Script version.
    """
    path: PosixPath = Path(__file__)
    this_symlink: str = str(path.parent)
    this_folder: PosixPath = Path(this_symlink).resolve()
    this_version: str = get_version_from_folder_name(this_folder,
                                                     script_folder_name)
    return this_version


def get_output_filename(args: argparse.Namespace) -> str:
    """Get the output filename.

    If an output filename is set in the CLI arguments, use it. Otherwise,
    derive it from the name of the input file. Defaults to
    INPUT_FILE.OUTPUT.xyz

    Args:
        args (argparse.Namespace): Parsed CLI arguments.

    Returns:
        str: Output filename.
    """
    if args.output:
        return args.output

    file_name: str
    file_extension: str
    file_name, file_extension = os.path.splitext(args.input_file)
    return file_name + ".OUTPUT" + file_extension


def get_cli_command(global_script_name: str, sys_args: List[str],
                    script_version: str) -> str:
    """Get the CLI command to reproduce the results of the current run.

    Args:
        global_script_name (str): Name of the symlink in the PATH that points
                                  to the OTP API script.
        sys_args (List[str]): CLI arguments.
        script_version (str): Version of the script.

    Returns:
        str: Full CLI command to reproduce the results.
    """
    arguments: str
    if "--version" not in sys_args:
        arguments = "--version " + str(script_version) + " " + \
                    " ".join(sys_args)
    elif sys_args[sys_args.index("--version") + 1] == "latest":
        version_index: int = sys_args.index("--version")
        arguments = " ".join(sys_args[:version_index + 1] +
                             [script_version] + sys_args[version_index + 2:])
    else:
        arguments = " ".join(sys_args)
    command: str = "$ " + global_script_name + " " + arguments
    return command


def parse_header(input_filename: str, end_settings: str,
                 comment: str) -> Tuple[List[str], str]:
    """Extract settings and data header from the data file.

    Args:
        input_filename (str): Name of the file to open.
        end_settings (str): String flag that indicates the end of the settings
                            block.
        comment (str): Character(s) used for comments in the data file.

    Returns:
        Tuple[List[str], str]: Settings and data header.
    """
    settings: str = ""
    current_row: str = ""

    # pylint: disable=invalid-name
    f: IO
    with open(input_filename) as f:
        # append until end of settings
        while end_settings not in current_row:
            current_row = f.readline()
            settings += current_row

        # append until first non-comment non-empty line: the data header
        current_row = f.readline()
        while (current_row.strip().startswith(comment) or
               current_row.strip() == ""):
            settings += current_row
            current_row = f.readline()

        data_header: str = current_row.strip()

    return settings, data_header


def file_to_params(file_path: str, comment: str, end_settings: str,
                   config: configparser.SectionProxy) -> Dict[str, str]:
    """Generate data row Dicts from a file.

    Args:
        file_path (str): Path of the data file to open.
        comment (str): Character(s) used for comments in the data file.
        end_settings (str): String flag that indicates the end of the settings
                            block.
        config (configparser.SectionProxy): Config object containing data file
                                            settings.

    Yields:
        Dict[str, str]: Dict containing settings and URL for a certain data
                        row.
    """
    with open(file_path) as f:  # pylint: disable=invalid-name
        # Discard lines until we encounter the end of the settings block.
        settings_line: str = comment
        while end_settings not in settings_line:
            settings_line = f.readline()

        header: str = ""
        # Discard lines until one is not empty
        # or does not start with a comment character.
        # This will be the header.
        while header.startswith(comment) or header == "":
            header = f.readline().strip()

        data_line: str
        for data_line in f:
            # Skip comments.
            if data_line.strip().startswith(comment):
                continue
            # Skip empty lines.
            if data_line.strip() == "":
                continue

            url_params: List[Tuple[str]] = []
            line_array: List[str] = \
                data_line.strip('\n').split(json.loads(config["separator"]))

            index: int
            param_name: str
            param: str
            for index, param_name in \
                    enumerate(header.split(json.loads(config["separator"]))):
                if param_name in config["non_url_parameters"]:
                    continue
                if param_name in config["split_these_parameters"]:
                    for param in line_array[index].split(','):
                        url_params.append((param_name, param))
                else:
                    url_params.append((param_name, line_array[index]))
            data_row: Dict[str, str] = {"data": data_line,
                                        "url": assemble_url(url_params,
                                                            config),
                                        "url_params": url_params}
            yield data_row


def filter_duration(response_list: List[str]):
    """Filter API query results for travel durations to reduce data volume.

    Args:
        response_list (List[str]): List of API query results in JSON format.

    Returns:
        str: Duration of a trip, using "-1" for errors.
    """
    duration_list: List[str] = []
    line: str
    for line in response_list:
        try:
            line_dict = json.loads(line)
            duration = str(line_dict["plan"]["itineraries"][0]["duration"])
        # pylint: disable=invalid-name, broad-except
        except Exception as e:
            logger.warning(e)
            duration = "-1"
        duration_list.append(duration)
    return duration_list


async def get_response(index: int, url: str,
                       session: aiohttp.ClientSession, **kwargs) -> set:
    """Fetch HTML response from a website or API.

    Modified from areq.py

    Args:
        index (int): Index of the query, just to keep track of order.
        url (str): Source to fetch from.
        session (aiohttp.ClientSession): aiohttp session to use.

    Returns:
        Tuple[int, str]: Tuple of the index and HTML response.
    """
    try:
        html: str = await areq.fetch_html(url=url, session=session, **kwargs)

    # Catch aiohttp errors
    # pylint: disable=invalid-name
    except (aiohttp.ClientError,
            aiohttp.http_exceptions.HttpProcessingError) as e:
        error: Tuple[str] = ("aiohttp exception for %s [%s]: %s",
                             url,
                             getattr(e, "status", None),
                             getattr(e, "message", None))
        logger.error(*error)
        return index, str(error)

    # Catch other errors
    # pylint: disable=invalid-name, broad-except
    except Exception as e:
        exception: Tuple[str] = ("Non-aiohttp exception occured:  %s",
                                 getattr(e, "__dict__", {}))
        logger.exception(*exception)
        return index, str(exception)
    else:
        return index, html


async def bulk_requests(urls: list) -> List[Tuple[int, str]]:
    """Bunch requests to process them in bulk.

    Args:
        urls (List[Tuple[int, str]]): List of URLs to query asynchroneously.

    Returns:
        List[Tuple[int, str]]: List of (index, response) tuples.
    """
    session: aiohttp.client.ClientSession
    async with aiohttp.ClientSession() as session:

        tasks: List[Coroutine] = []

        index: int
        url: str
        for index, url in urls:
            tasks.append(get_response(index=index, url=url, session=session))

        results: List[Tuple[int, str]] = await asyncio.gather(*tasks)
    return results


def async_query(iter_data: Iterator[Dict[str, str]],
                config: configparser.SectionProxy) -> List[str]:
    """Query an API asynchroneously.

    Get results and restore the order that might be lost through asynchroneous
    querying. Filter results if requested.

    Args:
        iter_data (Iterator[Dict[str, str]]): Generator containing data row
                                              dicts.
        config (configparser.SectionProxy: Dict-style config object.

    Yields:
        List[str]: List of query results.
    """
    separator: str = json.loads(config["separator"])
    max_chunk_size: int = json.loads(config["max_chunk_size"])

    while True:
        done: bool = False
        current_batch_numbered: List[Tuple[int, Dict[str, str]]] = []
        try:
            for _ in range(max_chunk_size):
                numbered_iter_data: Tuple[int, Dict[str, str]] = \
                    enumerate(iter_data)
                current_batch_numbered.append(next(numbered_iter_data))
        except StopIteration:
            done = True

        if current_batch_numbered:
            numbered_urls: List[Tuple[int, str]] = \
                [(index, data_row["url"])
                 for (index, data_row) in current_batch_numbered]
            numbered_response_list: List[Tuple[int, str]] = \
                asyncio.run(bulk_requests(numbered_urls))

            # Restore order that might be messed up by asynchroneous queries.
            numbered_response_list.sort(key=lambda x: x[0])

            # Strip index. After restoring order it's not needed anymore.
            response_list: List[str] = list(zip(*numbered_response_list))[1]

            # Filter results if requested.
            if "duration-only" in json.loads(config["filters"]):
                response_list = filter_duration(response_list)

            original_data_list: List[str] = \
                [data_row["data"] for _, data_row in current_batch_numbered]
            url_list: List[str] = [data_row["url"] for _, data_row in
                                   current_batch_numbered]
            data_batch: List[Tuple[str]] = \
                list(zip(original_data_list, url_list, response_list))
            separated_batch: List[str] = \
                [orig_data.strip() + separator + url + separator + response
                 for orig_data, url, response in data_batch]

            yield separated_batch

        if done:
            break
