# OpenTripPlanner API script

This script enables batch-queries to an [http://www.opentripplanner.org/](OpenTripPlanner API) instance. 

## Installation
Put a symlink to the `otp` file in a place that's in your PATH, for example `/usr/local/bin`.
Within the `otp` file, change the `SCRIPT_LOCATION` to the location of this repository.
If you want to avoid having the user to install packages, put the following packages inside the `otp_api_script_X` folders:

- `aiofiles`
- `aiohttp`
- `async_timeout`
- `multidict`
- `packaging`
- `yarl`

## Usage

```bash
usage: otp [-h] [-o FILE] [-t] [--version VERSION] [--print_versions] INPUT_FILE

Make parallel calls to the OpenTripPlanner API.

positional arguments:
  INPUT_FILE            name of the data file to parse

optional arguments:
  -h, --help            show this help message and exit
  -o FILE, --output FILE
                        name of the output file; default is
                        INPUT_FILE.OUTPUT.xyz in the same folder as the input
                        file.
  -t, --test            only assemble and print the URLs
  --version VERSION     which version to run, defaults to 'latest'
  --print_versions      print all available versions and exit
```

## Data file format

Consult the [OpenTripPlanner Documentation](http://docs.opentripplanner.org/en/latest/) for URL parameters.

Example file:

```
# DATA FILE FOR OPENTRIPPLANNER QUERIES

# Rows starting with a comment character (here '#') are ignored.

### START SETTINGS ### (do not edit or omit this line)

# Description for this run. Important for documentation purposes.
# You can use multiple lines with indentation. 
# description:
#   one line
#   another line
description:
  abcde
  fghij

# What kind of task to do, route planning "plan" or "isochrone"
target: "plan"

# Which version of the dataset to use? E.g. "nl_2021_01", "nl_2020_06"
router: "nl_2021_01"

# Filter output to preserve storage space, comma separated list.
# Available filters:
# - "duration-only": Only store travel durations of the first itinerary
filters: ["duration-only"]

# All comma-separated parameters in this list will be split. Example:
# cutoffSec 100,200 -> cutoffSec=100&cutoffSec=200
#
# All others will be kept as a single entry. Example:
# fromPlace 51.03804,5.83665 -> fromPlace=51.03804,5.83665
split_these_parameters: ["cutoffSec"]

# Parameters in this list will not be used in the URL. This can be for
# example the ID of a data row.
non_url_parameters: ["ID"]

# Separator for header and data lines, may be more than only one charater.
separator: " | "

# Behind which port OTP is found. Different OTP versions will use different
# ports.
# OTP versions:
# 1.4.0 port: 31002
port: 31002

# Which protocol to use, http or https
protocol: "http"

# What is the address of the server? Use "localhost" if the API runs on the
# same machine
server: "localhost"

# Location of the router, don't touch this unless you know what you are doing
stub: "/otp/routers/"

# How many queries to send at the same time, before collecting the results?
# 240 seems to be a good default value.
max_chunk_size: 240

### END SETTINGS ### (do not edit or omit this line)

# The first non-comment data line is the header. It contains the names
# of all the parameters used in the query.

# HEADER:
ID | fromPlace | toPlace | mode | maxWalkDistance

# DATA LINES:
1 | 52.11670630883542,4.65126412339596 | 52.12775723892858,4.632164234964834 | CAR | 500
2 | 52.12775723892858,4.632164234964834 | 52.11670630883542,4.65126412339596 | CAR | 500
```

## Development

Every version lives in it's own folder `otp_api_script_VERSION`.
Once you release a version, write protect it and don't touch it anymore; older, even flawed, versions are kept for reproducability.
Use [Semantic Versioning](https://semver.org/spec/v2.0.0.html) for the version numbers.
Let `otp_api_script_latest` point to the latest version to use by default.
